package com.daparcoc.laboratorio53_aplicacion1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
    private ListView lv;
    public static final String[] COMIDAS = {
        "arroz con pollo:\n" +
            "- 8 presas de pollo\n" +
            "- 1/2 taza de aceite\n" +
            "- 1 cebolla mediana picada en cuadritos\n" +
            "- 3 dientes de ajo picados\n" +
            "...\n" +
            "Tiempo de cocción: 30 minutos.",
        "aji de gallina:\n" +
            "- 1 gallina de 2 kilos aprox.\n" +
            "- 1/4 de kilo de cebolla picada\n" +
            "- 1/2 cucharada de ajos molidos\n" +
            "- 6 ajíes verdes licuados\n" +
            "...\n" +
            "Tiempo de cocción: 20 minutos",
        "lomo saltado:\n" +
            "- 1/2 kilo de lomo de res\n" +
            "- 1 cebolla grande\n" +
            "- 1 tomate grande\n" +
            "- ajos molidos, sal, pimienta\n" +
            "...\n" +
            "Tiempo de cocción: 25 minutos",
        "tacu tacu: \n" +
            "- 1/2 kilo de frejol canario\n" +
            "- 1/2 kilo de manteca de chancho en cubitos\n" +
            "- 1/2 kilo de arroz cocido graneado\n" +
            "- 2 cdas. de aceite\n" +
            "...\n" +
            "Tiempo de cocción: 35 minutos",
        "ceviche: \n" +
            "- 200 g de pescado blanco fresco\n" +
            "- 1/2 cebolla mediana\n" +
            "- 1/2 cucharada de sal\n" +
            "- ají limo (de preferencia rojo y amarillo)\n" +
            "...\n" +
            "Tiempo de cocción: 10 minutos",
        "hostia que hambre me ha dado"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lv = (ListView) findViewById(R.id.LstOpciones);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, COMIDAS);
        lv.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
